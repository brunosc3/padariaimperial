﻿using Cadastros.Classes;
using Cadastros.Persistencia;
using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastrarProduto_Alterar : System.Web.UI.Page
{
    private bool IsGerente(int tipo)
    {
        bool retorno = false;
        if (tipo == 0)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ProdutoBD bd = new ProdutoBD();
            Produto produto = bd.Select(Convert.ToInt32(Session["ID"]));
            txtNome.Text = produto.Nome;
        }        {
            int codigo = Convert.ToInt32(Session["CODIGO"]);
            FuncionarioBD bd = new FuncionarioBD();
            Funcionario funcionario = bd.Select(codigo);
            if (!IsGerente(funcionario.Tipo))
            {
                Response.Redirect("../Erro/AcessoNegado.aspx");
            }
            else
            {
                lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
            }
        }
    }


    protected void btnSalvarAlteracao_Click(object sender, EventArgs e)
    {
        ProdutoBD bd = new ProdutoBD();
        Produto funcionario = bd.Select(Convert.ToInt32(Session["ID"]));
        funcionario.Nome = txtNome.Text;
        if (bd.Update(funcionario))
        {
            lblMensagem.Text = "Alteração de produto realizada.";
            txtNome.Focus();
        }
        else
        {
            lblMensagem.Text = "Não foi possível alterar.";
        }
    }
    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}