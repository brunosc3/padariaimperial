﻿using Cadastros.Classes;
using Cadastros.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using PadariaImperial.Persistencia;
using PadariaImperial.Classes;

public partial class Paginas_CadastrarMateriaPrima_Cadastrar : System.Web.UI.Page
{
    private bool IsGerente(int tipo)
    {
        bool retorno = false;
        if (tipo == 0)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        int codigo = Convert.ToInt32(Session["CODIGO"]);
        FuncionarioBD bd = new FuncionarioBD();
        Funcionario funcionario = bd.Select(codigo);
        if (!IsGerente(funcionario.Tipo))
        {
            Response.Redirect("../Erro/AcessoNegado.aspx");
        }
        else
        {
            lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        MateriaPrima materiaprima = new MateriaPrima();
        materiaprima.Nome = txtNome.Text;
        materiaprima.QuantidadeMinima = Convert.ToDouble(txtQtdMin.Text);
        materiaprima.Fornecedor = txtFornecedor.Text;
        materiaprima.Saldo = Convert.ToDouble(txtSaldo.Text);
        MateriaPrimaBD bd = new MateriaPrimaBD();
        if (bd.Insert(materiaprima))
        {
            lblMensagem.Text = "Nova matéria prima cadastrada";
            txtNome.Text = "";
            txtQtdMin.Text = "";
            txtFornecedor.Text = "";
            txtSaldo.Focus();
        }
        else
        {
            lblMensagem.Text = "Erro ao cadastrar.";
        }

    }

    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}