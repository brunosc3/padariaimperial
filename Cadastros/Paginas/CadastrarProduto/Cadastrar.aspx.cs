﻿using Cadastros.Classes;
using Cadastros.Persistencia;
using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastrarProduto : System.Web.UI.Page
{
    private bool IsGerente(int tipo)
    {
        bool retorno = false;
        if (tipo == 0)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        int codigo = Convert.ToInt32(Session["CODIGO"]);
        FuncionarioBD bd = new FuncionarioBD();
        Funcionario funcionario = bd.Select(codigo);
        if (!IsGerente(funcionario.Tipo))
        {
            Response.Redirect("../Erro/AcessoNegado.aspx");
        }
        else
        {
            lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
        }
    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {
        Produto produto = new Produto();
        produto.Nome = txtNomeProduto.Text;
        ProdutoBD bd = new ProdutoBD();
        if (bd.Insert(produto))
        {
            lblMensagem.Text = "Produto Cadastrado!";
            txtNomeProduto.Text = "";
            
            txtNomeProduto.Focus();
        }
        else
        {
            lblMensagem.Text = "Não foi possível cadastrar o produto.";
        }
    }

    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}