﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PadariaImperial.Classes
{
    /// <summary>
    /// Descrição resumida de Funcionario
    /// </summary>
    public class Funcionario
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public int Tipo { get; set; }

        public Funcionario()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}