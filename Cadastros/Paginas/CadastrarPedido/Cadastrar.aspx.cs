﻿using Cadastros.Classes;
using Cadastros.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using PadariaImperial.Persistencia;
using PadariaImperial.Classes;

public partial class Paginas_CadastrarPedido_Cadastrar : System.Web.UI.Page
{
    private bool IsBalconista(int tipo)
    {
        bool retorno = false;
        if (tipo == 1)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        Carregarddl();
        {
            int codigo = Convert.ToInt32(Session["CODIGO"]);
            FuncionarioBD bd = new FuncionarioBD();
            Funcionario funcionario = bd.Select(codigo);
            if (!IsBalconista(funcionario.Tipo))
            {
                Response.Redirect("../Erro/AcessoNegado.aspx");
            }
            else
            {
                lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
            }
        }
    }

    private void Carregarddl()
    {
        ProdutoBD p = new ProdutoBD();
        DataSet ds = p.SelectAll();
        ddlProdutos.DataSource = ds.Tables[0].DefaultView;
        ddlProdutos.DataTextField = "pro_nome";
        ddlProdutos.DataValueField = "pro_codigo";
        ddlProdutos.DataBind();
        ddlProdutos.Items.Insert(0, new ListItem("Selecione", "0"));
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Pedido pedido = new Pedido();
        pedido.Nome = Convert.ToString(ddlProdutos.SelectedItem.Text);
        pedido.Quantidade = Convert.ToDouble(txtQuantidade.Text);
        pedido.Balconista = txtBalconista.Text;
        pedido.Produto_Codigo = Convert.ToInt32(txtCod.Text);
        PedidoBD bd = new PedidoBD();
        if (bd.Insert(pedido))
        {
            lblMensagem.Text = "Pedido cadastrado";
            txtQuantidade.Text = "";
            txtBalconista.Text = "";
            txtCod.Focus();
        }
        else
        {
            lblMensagem.Text = "Erro ao cadastrar.";
        }

    }
    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}