﻿using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Login_Login : System.Web.UI.Page
{
    private bool IsPreenchido(string str)
    {
        bool retorno = false;
        if (str != string.Empty)
        {
            retorno = true;
        }
        return retorno;
    }
    private bool UsuarioEncontrado(Funcionario funcionario)
    {
        bool retorno = false;
        if (funcionario != null)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnEntrar_Click(object sender, EventArgs e)
    {
        string nome = txtNome.Text.Trim();
        string senha = txtSenha.Text.Trim();
        if (!IsPreenchido(nome))
        {
            lblMensagem.Text = "Preencha o Nome";
            txtNome.Focus();
            return;
        }
        if (!IsPreenchido(senha))
        {
            lblMensagem.Text = "Preencha a senha";
            txtSenha.Focus();
            return;
        }
        FuncionarioBD bd = new FuncionarioBD();
        Funcionario funcionario = new Funcionario();
        funcionario = bd.Autentica(nome, senha);
        if (!UsuarioEncontrado(funcionario))
        {
            lblMensagem.Text = "Usuário não encontrado";
            txtNome.Focus();
            return;
        }
        Session["CODIGO"] = funcionario.Codigo;
        switch (funcionario.Tipo)
        {
            case 0:
                Response.Redirect("Gerente/Home.aspx");
                break;
            case 1:
                Response.Redirect("Balconista/Home.aspx");
                break;
            default:
                break;
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        string nome = txtNome.Text.Trim();
        string senha = txtSenha.Text.Trim();
        if (!IsPreenchido(nome))
        {
            lblMensagem.Text = "Preencha o Nome";
            txtNome.Focus();
            return;
        }
        if (!IsPreenchido(senha))
        {
            lblMensagem.Text = "Preencha a senha";
            txtSenha.Focus();
            return;
        }
        FuncionarioBD bd = new FuncionarioBD();
        Funcionario funcionario = new Funcionario();
        funcionario = bd.Autentica(nome, senha);
        if (!UsuarioEncontrado(funcionario))
        {
            lblMensagem.Text = "Usuário não encontrado";
            txtNome.Focus();
            return;
        }
        Session["CODIGO"] = funcionario.Codigo;
        switch (funcionario.Tipo)
        {
            case 0:
                Response.Redirect("Gerente/Home.aspx");
                break;
            case 1:
                Response.Redirect("Balconista/Home.aspx");
                break;
            default:
                break;
        }
    }
}
