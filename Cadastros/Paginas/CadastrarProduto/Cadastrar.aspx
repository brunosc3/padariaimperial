﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Cadastrar.aspx.cs" Inherits="Paginas_CadastrarProduto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        header{
            background-color: #00C162;
            background-attachment: fixed;
            background-position: center;
            margin-top: 0px;
            text-align: center;
        }
        .auto-style1 {
            height: 150px;
            margin-top: 0px;
            text-align: left;
        }
        menu{

        }
        .auto-style3 {
            height: 20px;
            text-align: justify;
        }
        nav{
            margin-left: 0px;
        }
        .auto-style4 {
            width: 960px;
            height: 373px;
            text-align: left;
        }
        .auto-style6 {
            width: 150px;
            height: 120px;
            float: none;
            margin: 10px;
        }
		#esquerda{
			width: 50%;
			float: left;
			
		}
		#direita{
			width: 50%;
			float: right;
			
		}
        .auto-style7 {
            height: 320px;
            width: 34%;
        }
        .auto-style8 {
            width: 43%;
            height: 339px;
            text-align: center;
        }
    </style>
</head>
<body style="margin-left: 0px; margin-right: 0px; margin-top: 0px; height: 497px;";>
    <form id="form1" runat="server">
        <header id="Topo" class="auto-style1">
            <img src="../SupImperial.jpeg" class="auto-style6" style="border: thin solid #FFFFFF;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblTitulo0" runat="server" ForeColor="#003300"></asp:Label>
&nbsp;<asp:Button ID="lbSair" runat="server" BackColor="White" ForeColor="#006600" Height="21px" OnClick="lbSair_Click" Text="Sair" Width="37px" />
        </header>
        <nav id="menu" class="auto-style3" style="border-color: #00C162; border-width: medium; border-top-style: solid; border-right-style: none; border-bottom-style: solid; border-left-style: none;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Menu
        </nav>
        <header class="auto-style4" style="background-color: #FFFFFF">
		<div id="esquerda" class="auto-style7">
            <br />
&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblVizualizar" runat="server" Text="Vizualizar" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL1" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/CadastrarMateriaPrima/Listar.aspx">Estoque </asp:HyperLink>
		    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL2" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/CadastrarProduto/Listar.aspx">Produtos</asp:HyperLink>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL3" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/CadastrarPedido/Listar.aspx">Pedidos</asp:HyperLink>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblCadastrar" runat="server" Text="Cadastrar" BackColor="White" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL4" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/CadastrarProduto/Cadastrar.aspx">Produtos</asp:HyperLink>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL5" runat="server" NavigateUrl="~/Paginas/CadastrarMateriaPrima/Cadastrar.aspx" EnableTheming="True" ForeColor="#006600">Matéria Prima</asp:HyperLink>
            <br />
		</div>
		<div id="direita" class="auto-style8">
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
&nbsp;<asp:Label ID="lblTitulo" runat="server" Text="CADASTRO DE PRODUTO"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblNomeProduto" runat="server" Text="Nome do produto:"></asp:Label>
            <br />
            <asp:TextBox ID="txtNomeProduto" runat="server" Width="250px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
            &nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            <br />
			</div>
        </header>
     </form>
</body>
</html>
