﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadastros.Classes
{
    /// <summary>
    /// Summary description for Funcionario
    /// </summary>
    public class MateriaPrima
    {
        //propriedades
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public double QuantidadeMinima { get; set; }
        public string Fornecedor { get; set; }
        public double Saldo { get; set; }

        //construtor
        public MateriaPrima()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}