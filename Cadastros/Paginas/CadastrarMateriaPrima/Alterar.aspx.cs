﻿using Cadastros.Classes;
using Cadastros.Persistencia;
using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastrarMateriaPrima_Alterar : System.Web.UI.Page
{
    private bool IsGerente(int tipo)
    {
        bool retorno = false;
        if (tipo == 0)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MateriaPrimaBD bd = new MateriaPrimaBD();
            MateriaPrima materiaprima = bd.Select(Convert.ToInt32(Session["ID"]));
            txtNome.Text = materiaprima.Nome;
            txtQtdMin.Text = materiaprima.QuantidadeMinima.ToString();
            txtFornecedor.Text = materiaprima.Fornecedor;
            txtSaldo.Text = materiaprima.Saldo.ToString();

        }        {
            int codigo = Convert.ToInt32(Session["CODIGO"]);
            FuncionarioBD bd = new FuncionarioBD();
            Funcionario funcionario = bd.Select(codigo);
            if (!IsGerente(funcionario.Tipo))
            {
                Response.Redirect("../Erro/AcessoNegado.aspx");
            }
            else
            {
                lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
            }
        }
    }

    protected void btnSalvarAlteracao_Click(object sender, EventArgs e)
    {
        MateriaPrimaBD bd = new MateriaPrimaBD();
        MateriaPrima materiaprima = bd.Select(Convert.ToInt32(Session["ID"]));
        materiaprima.Nome = txtNome.Text;
        materiaprima.QuantidadeMinima = Convert.ToDouble(txtQtdMin.Text);
        materiaprima.Fornecedor = txtFornecedor.Text;
        materiaprima.Saldo = Convert.ToDouble(txtSaldo.Text);



        if (bd.Update(materiaprima))
        {
            lblMensagem.Text = "Alteração da materia prima realizada.";
            txtNome.Focus();
        }
        else
        {
            lblMensagem.Text = "Não foi possível alterar.";
        }
    }
    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}