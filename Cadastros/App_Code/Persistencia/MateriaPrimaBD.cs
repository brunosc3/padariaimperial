﻿using FATEC; //para acesso a classe Mapped
using System;
using System.Web;
using Cadastros.Classes; //para acesso a classe Funcionario
using System.Data; //para uso de DataSet
namespace Cadastros.Persistencia
{
    /// <summary>
    /// Summary description for FuncionarioBD
    /// </summary>
    public class MateriaPrimaBD
    {
        //métodos
        //insert
        public bool Insert(MateriaPrima materiaprima)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "INSERT INTO tbl_materiaprima(mtp_nome, mtp_qtdmin, mtp_fornecedor, mtp_saldo) VALUES (?nome, ?qtdmin, ?fornecedor, ?saldo)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?nome", materiaprima.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?qtdmin", materiaprima.QuantidadeMinima));
            objCommand.Parameters.Add(Mapped.Parameter("?fornecedor", materiaprima.Fornecedor));
            objCommand.Parameters.Add(Mapped.Parameter("?saldo", materiaprima.Saldo));
            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }
        //selectall
        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataAdapter objDataAdapter;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_materiaprima", objConexao);
            objDataAdapter = Mapped.Adapter(objCommand);
            objDataAdapter.Fill(ds);
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return ds;
        }
        //select
        public MateriaPrima Select(int id)
        {
            MateriaPrima obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_materiaprima WHERE mtp_codigo = ?codigo", objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new MateriaPrima();
                obj.Codigo = Convert.ToInt32(objDataReader["mtp_codigo"]);
                obj.Nome = Convert.ToString(objDataReader["mtp_nome"]);
                obj.QuantidadeMinima = Convert.ToDouble(objDataReader["mtp_qtdmin"]);
                obj.Fornecedor = Convert.ToString(objDataReader["mtp_fornecedor"]);
                obj.Saldo = Convert.ToDouble(objDataReader["mtp_saldo"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }
        //update
        public bool Update(MateriaPrima materiaprima)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "UPDATE tbl_materiaprima SET mtp_nome=?nome, mtp_qtdmin=?qtdmin, mtp_fornecedor=?fornecedor, mtp_saldo=?saldo WHERE mtp_codigo=?codigo";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", materiaprima.Codigo));
            objCommand.Parameters.Add(Mapped.Parameter("?nome", materiaprima.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?qtdmin", materiaprima.QuantidadeMinima));
            objCommand.Parameters.Add(Mapped.Parameter("?fornecedor", materiaprima.Fornecedor));
            objCommand.Parameters.Add(Mapped.Parameter("?saldo", materiaprima.Saldo));
            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }
        //delete
        public bool Delete(int id)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "DELETE FROM tbl_materiaprima WHERE mtp_codigo=?codigo";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));

            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }
        //construtor
        public MateriaPrimaBD()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}